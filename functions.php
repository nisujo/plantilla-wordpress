<?php

/**
 *  Activar soporte de imagenes para posts.
 */
add_theme_support('post-thumbnails');

/**
 * Generar url de navegación.
 */
function url($value)
{
    return get_site_url() . '/' . $value;
}

/**
 * Generar url que apunte a un asset de la plantilla.
 */
function url_asset($value)
{
    return get_stylesheet_directory_uri() . '/' . $value;
}

/**
 * Cargar scripts y estilos del tema.
 */
function theme_enqueue_scripts()
{
    wp_enqueue_style( 'normalize', url_asset('assets/libs/normalize/normalize.css') );
    // wp_enqueue_style( 'slick-css', url_asset('assets/libs/slick-1.8.1/slick/slick.css') );
    // wp_enqueue_style( 'slick-theme', url_asset('assets/libs/slick-1.8.1/slick/slick-theme.css') );
    wp_enqueue_style( 'style', get_stylesheet_uri() );
    // wp_enqueue_script( 'slick-js', url_asset('assets/libs/slick-1.8.1/slick/slick.min.js') );
    wp_enqueue_script( 'script', url_asset('script.js') );
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );

/**
 * Escribir registro en el archivo debug.log.
 */
if (!function_exists('write_log')) {

    function write_log( $log )
    {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }

}

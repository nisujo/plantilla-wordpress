# Tema de Wordpress
Desarrollado por: [Grupo Samant](https://gruposamant.com)

## Manipular Assets
En el archivo de configuración **functions.php** se puede ver que los assets que por defecto carga este tema son (también carga jquery por defecto):

```
wp-content/themes/theme-name/assets/libs/normalize/normalize.css
wp-content/themes/theme-name/style.css
wp-content/themes/theme-name/script.js
```

### Modificar CSS
Para editar los estilos de la pagina no se debe tocar el archivo style.css, todas las modificaciones deben hacerse en los archivos de la carpeta **assets/css/**

### Modificar JS
Para editar los scripts de javascript de la pagina no se debe tocar el archivo script.js, todas las modificaciones deben hacerse en los archivos de la carpeta **assets/js/**

### Compilar Assets
Los archivos que estan en la carpeta **assets/css** y **assets/js** son compilados y agrupados en un solo archivo **style.css** y **script.js**. Para compilar se ejecuta el script **compile_assets.php** ya sea por consola o desde un navegador.

Compilar desde consola
```
php compile_assets.php
```

Compilar desde el navegador
```
http://miweb.com/wp-content/themes/mi-tema/compile_assets.php
```

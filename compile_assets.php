<?php

function getFilesContent($folder, $main_file, $extension)
{
    $files = scandir($folder, 0);
    $css_content = "";

    foreach ($files as $file) {
        if (strpos($file, $extension) > 0) {

            if ($file !== $main_file) {
                $css_content .= "\n\n";
                $css_content .= "/******** Inicio: " . $file . " ********/";
                $css_content .= "\n\n";
            }

            $css_content .= file_get_contents($folder . $file);

            if ($file !== $main_file) {
                $css_content .= "\n\n/******** Fin: " . $file . " ********/\n";
            }

        }
    }
    return $css_content;
}

function writeFile($file, $file_content)
{
    $write_file = fopen($file, 'w');

    fwrite($write_file, $file_content);
    fclose($write_file);
}

/**
 * Reescribir todo el archivo de estilos y scripts de wordpress.
 */
function writeAssets()
{
    writeFile( 'style.css', getFilesContent('assets/css/', '_style.css', '.css') );
    writeFile( 'script.js', getFilesContent('assets/js/', '_main.js', '.js') );
}

try {

    writeAssets();

} catch (Exception $ex) {

    echo $ex->getMessage();

}


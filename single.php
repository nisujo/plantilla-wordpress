<?php get_header(); ?>

<div class="wrapper">

    <main class="">

        <?php if ( has_post_thumbnail() ): ?>

            <?php the_post_thumbnail('full', [ 'class' => '' ]); ?>

        <?php endif; ?>

        <h1 class=""><?php the_title(); ?></h1>

        <?php while ( have_posts() ) : the_post(); ?>

            <article class="">

                <?php the_content(); ?>

            </article>

        <?php endwhile; wp_reset_query(); ?>

    </main>

</div>

<?php get_footer(); ?>
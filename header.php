<!DOCTYPE html>

<html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <title><?php wp_title( '·',TRUE,'right' ); bloginfo( 'name' ); ?></title>

        <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1">
        <link rel="shortcut icon" href="<?= get_stylesheet_directory_uri() ?>/images/icono.ico" />

        <?php wp_head(); ?>
    </head>

<body>

    <header class="header">
    </header>